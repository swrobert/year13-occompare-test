<?php

namespace App\Http\Controllers;

use App\Contracts\OccupationParser;
use App\Repositories\OccupationsRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class OccupationsController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $occparser;
    private $repository;

    /**
     * OccupationsController constructor.
     * @param OccupationParser $parser
     * @param OccupationsRepository $repository
     */
    public function __construct(OccupationParser $parser, OccupationsRepository $repository)
    {
        $this->occparser = $parser;
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->occparser->list();
    }

    /**
     * Compare two Occupations
     * @param Request $request
     * @return array
     */
    public function compare(Request $request)
    {
       $this->occparser->setScope('skills');
       $occupation_1 = $this->occparser->get($request->get('occupation_1'));
       $occupation_2 = $this->occparser->get($request->get('occupation_2'));
       // compare two occupations
       $match = $this->repository->getSkillsMatchingPercentage($occupation_1, $occupation_2);
       return [
            'occupation_1' => $occupation_1,
            'occupation_2' => $occupation_2,
            'match' => $match
       ];
    }
}
