<?php

namespace App\Repositories;

class OccupationsRepository
{
    /**
     *  getSkillsMatchingPercentage
     *
     * @param $occupation_1
     * @param $occupation_2
     * @return false|float|string
     */
    public function getSkillsMatchingPercentage($occupation_1, $occupation_2)
    {
        if (empty($occupation_1) || empty($occupation_2)) {
            return '0';
        }
        $occupations = array_merge($occupation_1, $occupation_2);
        $match_skills = $this->getUniqueSkills($occupations);
        return round(count($match_skills) / (((count($occupation_1) + count($occupation_2)) / 2) / 100), 2);
    }

    /**
     *  Get Unique Skills
     *
     * @param $occupations
     * @return array
     */
    private function getUniqueSkills($occupations)
    {
        $temp = [];
        $match_skills = [];
        foreach ($occupations as $skill) {
            if (!in_array($skill['label'], $temp)) {
                $temp[] = $skill['label'];
            } else {
                $match_skills[] = $skill['label'];
            }
        }
        return $match_skills;
    }

}